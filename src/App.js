import './App.css';
import Product from './components/Products'
import { Button, Modal } from 'react-bootstrap';

function App() {
  return (
    <div className="App">
      <Product />
    </div>
  );
}

export default App;
